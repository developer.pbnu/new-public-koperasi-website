import "antd/dist/antd.min.css";
import "../styles/globals.css";
import "../styles/index.css";
import "../i18n";
import "aos/dist/aos.css";
import { QueryClient, QueryClientProvider } from "react-query";
// import { ReactQueryDevtools } from "react-query/devtools";
import Schema from 'async-validator';
Schema.warning = function(){};

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="text-gray-700 text-xs md:text-sm font-app">
        <Component {...pageProps} />
      </div>
      {/* <ReactQueryDevtools initialIsOpen={false} /> */}
    </QueryClientProvider>
  );
}
export default MyApp;
