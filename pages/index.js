import React, { useEffect } from "react";
import Head from "next/head";
import { observer } from "mobx-react";
import LayoutApp from "../components/layouts/main";
import { MethodPayment } from "../components/pages/index/MethodPayment";
import { Hero } from "../components/pages/index/Hero";
import AOS from "aos";
import BlogsData from "../components/pages/index/BlogsData";
import { Visi } from "../components/pages/index/Visi";
import { useTranslation } from "react-i18next";

const Home = observer(() => {
  const { t } = useTranslation();
  useEffect(() => {
    AOS.init({
      duration: 1000,
      once: true,
    });
  }, []);
  return (
    <>
      <Head>
        <title>{t("title")}</title>
        <meta name="description" content={t("description")} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <LayoutApp>
        <Hero />
        <MethodPayment />
        <Visi />
        <BlogsData />
      </LayoutApp>
    </>
  );
});
export default Home;
