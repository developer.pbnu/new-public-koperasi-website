import { Image } from "antd";
import React from "react";
import LayoutApp from "../../components/layouts/main";
import { useGetSingleBlog } from "../../hooks/blogs";
import moment from "moment";

const BlogsSlug = ({ slug }) => {
  const { data: dataBlogs, isSuccess } = useGetSingleBlog(slug);
  const data = dataBlogs ? dataBlogs.data : [];
  return (
    <div>
      <LayoutApp>
        <div className="max-w-5xl mx-auto my-2 bg-white p-4 min-h-screen">
          <div className="flex justify-center mb-4">
            <Image preview={false} alt="image" src={data?.image_url} />
          </div>
          <h1 className="text-lg text-center font-bold">{data?.title}</h1>
          <p className="mt-2 text-xs md:text-xs text-center font-bold mb-4">
            {moment(data?.date).fromNow()}
          </p>
          <p>{data?.content}</p>
        </div>
      </LayoutApp>
    </div>
  );
};

BlogsSlug.getInitialProps = async ({ query }) => {
  const { slug } = query;
  return { slug };
};

export default BlogsSlug;
