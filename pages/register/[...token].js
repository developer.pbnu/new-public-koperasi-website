import React, { useEffect, useState } from "react";
import { Image, message } from "antd";
import {
  EmailShareButton,
  FacebookShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";
import {
  FaFacebookF,
  FaGooglePlusSquare,
  FaTelegramPlane,
  FaTwitter,
  FaWhatsapp,
} from "react-icons/fa";
import { usePaymentToken } from "../../hooks/payment";
import { useTranslation } from "react-i18next";
import { SelectLang } from "../../i18n";
import { useRouter } from "next/router";

const Token = ({ params }) => {
  const router = useRouter();
  const [token, setToken] = useState([]);
  const { token: tokennya } = router.query;

  useEffect(() => {
    if (tokennya) {
      setToken(tokennya);
    }
  }, [tokennya]);

  // const { token } = params;
  const { t, i18n } = useTranslation();

  const pay = (token) => {
    window?.snap?.pay(token, {
      onSuccess: function (result) {
        console.log("Successful payment!", result);
        navigate("/");
      },
      onPending: function (result) {
        console.log("Pending payment!", result);
      },
      onError: function (result) {
        console.log("Error!", result);
      },
    });
  };

  const [url, setUrl] = useState("");
  useEffect(() => {
    setUrl(window.location.href);
  }, []);

  return (
    <div className="flex justify-center items-center min-h-screen w-full relative">
      <div className="absolute top-10 right-10 bg-slate-200 p-2 rounded-lg">
        <SelectLang i18nHook={i18n} />
      </div>
      <Background />
      {token[1] === "mail" ? (
        <Mail token={token[0]} />
      ) : (
        <div className="z-30">
          <div className="flex justify-center">
            <button
              onClick={() => pay(token[0])}
              className="bg-gray-600 hover:bg-gray-800 py-2 px-4 text-white rounded"
            >
              <span>{t("payment.paySnap")}</span>
            </button>
          </div>
          <div className="mt-10 flex items-center justify-center gap-2">
            <p className="text-center">{t("payment.saveUrl")}</p>
            <button
              className="bg-slate-200 py-1 px-2 rounded"
              onClick={() => {
                message.success("Copied to clipboard");
                navigator.clipboard.writeText(url);
              }}
            >
              {t("payment.copy")}
            </button>
          </div>
          <Share />
        </div>
      )}
    </div>
  );
};

const Mail = ({ token }) => {
  const { t } = useTranslation();
  const { data: dataGet, isSuccess } = usePaymentToken(token);
  const data = isSuccess && dataGet.data;

  const Money = (data) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(data);
  };

  const pay = (token) => {
    console.log("token", token);
    window?.snap?.pay(token, {
      onSuccess: function (result) {
        console.log("Successful payment!", result);
        navigate("/");
      },
      onPending: function (result) {
        console.log("Pending payment!", result);
      },
      onError: function (result) {
        console.log("Error!", result);
      },
    });
  };

  return (
    <div className="relative m-4">
      <div className="bg-white p-4 border rounded-lg shadow-lg">
        <div className="flex justify-center">
          <Image
            preview={false}
            src={data?.photo_profile}
            alt="profile"
            className="w-32 h-32 rounded-full mb-4 bg-gray-200 p-2 mx-auto"
          />
        </div>
        <p className="mb-4">{t("payment.tanks")}</p>
        <div className="grid grid-cols-1 gap-1 md:gap-3 md:grid-cols-2 grid-flow-row-dense">
          <span>{t("payment.name")}</span>
          <span className="capitalize text-teal-700">{data?.name}</span>
          <span>NIK</span>
          <span className="text-teal-700">{data?.nik}</span>
          <span>{t("payment.phone")}</span>
          <span className="text-teal-700">{data?.phone_number}</span>
          <span>Email</span>
          <span className="text-teal-700">{data?.email}</span>
          <span>{t("payment.registered")}</span>
          <span className="text-teal-700">{data?.registered_at}</span>
        </div>
        <div>
          {data?.payment_details?.map((e, index) => {
            return (
              <div key={index} className="grid grid-cols-1 gap-1 md:gap-3 mt-1 md:mt-3 md:grid-cols-2 grid-flow-row-dense">
                <span key={index}>{e?.name}</span>
                <span className="text-teal-700">{Money(e?.price)}</span>
              </div>
            );
          })}
        </div>
        <div className="grid mt-3 grid-cols-1 md:grid-cols-2 grid-flow-row-dense">
          <span>Total</span>
          <span className="text-teal-700 font-bold ml-0 md:ml-2">{Money(data?.amount)}</span>
        </div>
        <div className="mt-8 flex justify-center">
          <button
            onClick={() => pay(data?.payment_token)}
            className="py-2 px-4 rounded-lg bg-red-800 hover:bg-black shadow-lg text-white uppercase tracking-widest"
          >
            {t("payment.payClick")}
          </button>
        </div>
      </div>
    </div>
  );
};
const Background = () => {
  return (
    <>
      <div className="z-0">
        <div className="bg-orange-400 rounded-full h-64 w-64 absolute top-40 -left-40" />
        <div className="bg-slate-400 rounded-full h-64 w-64 absolute top-[750px] right-52" />
        <div className="bg-red-400 rounded-full w-4 h-20 absolute top-[450px] right-20" />
        <div className="bg-gray-400 rounded-full w-[100px] h-5 absolute top-[250px] right-40" />
        <div className="bg-red-400 rounded-full w-2 h-48 absolute top-[500px] right-[110px]" />
        <div className="bg-slate-400 rounded-full w-2 h-48 absolute top-[600px] left-[110px]" />
        <div className="bg-red-400 rounded-full w-20 h-20 absolute top-[1000px] left-[110px]" />
      </div>
    </>
  );
};

const Share = () => {
  const [url, setUrl] = useState("");
  useEffect(() => {
    setUrl(window.location.href);
  }, []);

  const title = "Payment Registration";
  const shareUrl = url;
  return (
    <>
      <div className="flex gap-2 justify-center mt-4">
        <div>
          <FacebookShareButton url={shareUrl} quote={title}>
            <div className="p-2 rounded-full bg-slate-100 hover:bg-slate-200 shadow-lg">
              <FaFacebookF className="mx-auto w-6 h-6" />
            </div>
          </FacebookShareButton>
        </div>

        <div>
          <TwitterShareButton url={shareUrl} title={title}>
            <div className="p-2 rounded-full bg-slate-100 hover:bg-slate-200 shadow-lg">
              <FaTwitter className="mx-auto w-6 h-6" />
            </div>
          </TwitterShareButton>

          <div className="Demo__some-network__share-count">&nbsp;</div>
        </div>

        <div>
          <TelegramShareButton url={shareUrl} title={title}>
            <div className="p-2 rounded-full bg-slate-100 hover:bg-slate-200 shadow-lg">
              <FaTelegramPlane className="mx-auto w-6 h-6" />
            </div>
          </TelegramShareButton>

          <div className="Demo__some-network__share-count">&nbsp;</div>
        </div>

        <div>
          <WhatsappShareButton url={shareUrl} title={title} separator=":: ">
            <div className="p-2 rounded-full bg-slate-100 hover:bg-slate-200 shadow-lg">
              <FaWhatsapp className="mx-auto w-6 h-6" />
            </div>
          </WhatsappShareButton>

          <div className="Demo__some-network__share-count">&nbsp;</div>
        </div>
        <div>
          <EmailShareButton url={shareUrl} subject={title} body="body">
            <div className="p-2 rounded-full bg-slate-100 hover:bg-slate-200 shadow-lg">
              <FaGooglePlusSquare className="mx-auto w-6 h-6" />
            </div>
          </EmailShareButton>
        </div>
      </div>
    </>
  );
};
export default Token;
