import React, { useState, useEffect } from "react";
import { Button, Form, message } from "antd";
import aos from "aos";
import Head from "next/head";
import {
  useGetBirthPlace,
  useGetCity,
  useGetProvinces,
} from "../../hooks/address";
import LayoutGuest from "../../components/layouts/guest";
import { useTranslation } from "react-i18next";
import { useRouter } from "next/router";
import { usePostRegistration } from "../../hooks/registration";
import { Step1 } from "../../components/pages/register/Step1";
import { SelectLang } from "../../i18n";
import { Step2 } from "../../components/pages/register/Step2";
import { Step3 } from "../../components/pages/register/Step3";
import UploadFileImg from "../../components/utils/uploadFileImg";
import { observer } from "mobx-react-lite";
import { useNik } from "../../global/useNik";
import Link from "next/link";
import Zoom from "react-reveal/Zoom";

const Index = observer(() => {
  const [registerForm] = Form.useForm();
  const { t, i18n } = useTranslation();
  const router = useRouter();

  useEffect(() => {
    aos.init({
      duration: 1000,
    });
  }, []);
  // set birth place
  const [birth_place, setBirth_place] = useState(1);
  const { data: birthPlace, isSuccess: successBirthPlace } = useGetBirthPlace();
  // set provinces
  const [prov, setProv] = useState("11");
  const { data: dataProvincis, isSuccess: successProvinces } =
    useGetProvinces();
  // set city by province and set city code
  const [city, setCityCode] = useState("");
  const { data: dataCity, isSuccess: successCity, refetch } = useGetCity(prov);
  // set gender
  const [gender, setGender] = useState("male");
  // set error message
  const [errorStatus, setErrorStatus] = useState("");
  const [error, setError] = useState([]);
  // SetmandatorySavingCount
  const [mandatorySavingCount, SetmandatorySavingCount] = useState(3);
  // setKTP and Photo Profile
  const [ktp, setKtp] = useState("");
  const [photoProfile, setPhotoProfile] = useState("");
  // mutate registration
  const { mutate, isError } = usePostRegistration();
  // status registration success
  const [success, setSuccess] = useState(false);

  const onFinish = async () => {
    if (ktp === "" || photoProfile === "") {
      message.error("Lengkapi data terlebih dahulu");
    }
    try {
      const values = await registerForm.validateFields();
      console.log("dua", values);
      const formdata = {
        // step1
        nik: parseInt(useNik.nik === "" ? values.nik : useNik.nik),
        name: values.name,
        email: values.email,
        password: values.password,
        phone_number: `+62${values.phone_number}`,
        gender: gender,
        // step2
        birth_date: values.birth_date,
        birth_place: birth_place,
        religion: "islam",
        address: values.address,
        city_code: city,
        // step3
        mandatory_saving_count: mandatorySavingCount,
        ktp: ktp,
        profile_photo: photoProfile,
      };
      mutate(formdata, {
        onSuccess: (e) => {
          message.success("Register Success");
          if (useNik.nik) {
            router.push("/register/" + e.data.payment_token);
          } else {
            setSuccess(true);
          }
        },
        onError: (error) => {
          const e = error?.response?.data?.message;
          setErrorStatus(error?.response?.data?.error);
          const array = [];
          if (typeof e === "string") {
            array.push(e);
            setError(array);
          } else {
            setError(e);
          }
        },
      });
    } catch (e) {}
  };

  return (
    <>
      <Head>
        <title>Register {`${useNik.nik && "Invitation"}`}</title>
        <meta name="description" content={t("description")} />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <LayoutGuest>
        {success ? (
          <div className="w-full">
            <Success />
          </div>
        ) : (
          <>
            {useNik.nik && (
              <>
                <div className="bg-teal-600 bg-opacity-70 p-2 rounded mb-4 text-white max-w-3xl mx-auto sticky top-12 z-50">
                  <p className="text-center font-bold">NIK : {useNik.nik}</p>
                  <p className="text-center text-xs">
                    {t("register.validationSuccessNik")}
                  </p>
                </div>
              </>
            )}
            <div className="relative">
              {useNik.nik && (
                <div className="z-0">
                  <Background />
                </div>
              )}
              <div className="p-4 max-w-3xl mx-auto bg-white min-h-screen z-20 relative">
                <div className="flex justify-between">
                  <p className="mb-2 text-center font-bold text-lg">Register</p>
                  <div className="flex justify-center items-center pl-4 py-2 gap-2 bg-slate-100 rounded mb-4">
                    <SelectLang i18nHook={i18n} />
                  </div>
                </div>
                {errorStatus && <p>{errorStatus}</p>}
                {isError &&
                  error.map((dt, index) => {
                    return (
                      <div key={index} className="text-red-500 txt-xs">
                        <p>{dt}</p>
                      </div>
                    );
                  })}
                <div data-aos="fade-up">
                  <Form
                    form={registerForm}
                    name="register"
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    layout={"vertical"}
                    autoComplete="off"
                  >
                    <Step1
                      nik={useNik.nik === "" ? "" : useNik.nik}
                      setGender={setGender}
                    />
                    <Step2
                      birthPlace={successBirthPlace && birthPlace}
                      setBirth_place={setBirth_place}
                      prov={dataProvincis}
                      setProv={(e) => setProv(e)}
                      city={dataCity}
                      setCityCode={(e) => setCityCode(e)}
                      successCity={successCity}
                      successProvinces={successProvinces}
                    />
                    <Step3
                      setmandatory_saving_count={(e) =>
                        SetmandatorySavingCount(e)
                      }
                    />
                    <UploadFileImg
                      aspect={3}
                      setValue={setKtp}
                      label="Kartu tanda penduduk"
                      type="ktp"
                      name="ktp"
                    />
                    <UploadFileImg
                      aspect={2}
                      setValue={setPhotoProfile}
                      label="Photo Profile"
                      type="profile_photo"
                      name="profile_photo"
                    />
                    <Button
                      type="primary"
                      htmlType="submit"
                      size="large"
                      className="bg-green-600 hover:bg-green-700 rounded text-white"
                    >
                      Submit
                    </Button>
                  </Form>
                </div>
              </div>
            </div>
          </>
        )}
      </LayoutGuest>
    </>
  );
});

const Success = () => {
  const { t } = useTranslation();
  return (
    <Zoom>
      <div className="bg-white max-w-4xl mx-auto mt-20 p-6 rounded-lg mb-20">
        <h1 className="text-lg font-bold">{t("register.success.title")}</h1>
        <p className="mb-10">{t("register.success.subtitle")}</p>
        <p className="mb-5 font-bold underline text-green-600">
          {t("register.success.description")}
        </p>
        <Link href="/">
          <a className="bg-slate-200 py-2 px-4 hover:bg-slate-300 hover:text-gray-600 rounded">
            {t("register.success.button")}
          </a>
        </Link>
      </div>
    </Zoom>
  );
};

const Background = () => {
  return (
    <>
      <div className="z-0">
        <div className="bg-orange-400 rounded-full h-64 w-64 absolute top-40 -left-40" />
        <div className="bg-slate-400 rounded-full h-64 w-64 absolute top-[750px] right-52" />
        <div className="bg-red-400 rounded-full w-4 h-20 absolute top-[450px] right-20" />
        <div className="bg-gray-400 rounded-full w-[100px] h-5 absolute top-[250px] right-40" />
        <div className="bg-red-400 rounded-full w-2 h-48 absolute top-[500px] right-[110px]" />
        <div className="bg-slate-400 rounded-full w-2 h-48 absolute top-[600px] left-[110px]" />
        <div className="bg-red-400 rounded-full w-20 h-20 absolute top-[1000px] left-[110px]" />
      </div>
    </>
  );
};
export default Index;
