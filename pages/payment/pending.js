import Link from "next/link";
import React from "react";

const Pending = () => {
  return (
    <div className="flex justify-center items-center min-h-screen w-full p-10">
      <div>
        <h1 className="text-4xl text-center uppercase font-bold text-orange-500">
          Pending
        </h1>
        <h1 className="text-center capitalize font-bold">
          Transaksi anda sedang ditunda. Mohon tunggu beberapa saat lagi, kami akan mengirimkan email konfirmasi balasan
        </h1>
        <div className="flex justify-center mt-10">
          <Link href="/">
            <a className="text-center underline text-blue-500">
              Kembali ke halaman utama
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Pending;
