import Link from "next/link";
import React from "react";

const Failed = () => {
  return (
    <div className="flex justify-center items-center min-h-screen w-full p-10">
      <div>
        <h1 className="text-4xl text-center uppercase font-bold text-red-500">
          Failed
        </h1>
        <h1 className="text-center capitalize font-bold">
          Mohon maaf, transaksi anda gagal. Coba lagi mungkin karena server
          payment sedang maintenance
        </h1>
        <div className="flex justify-center mt-10">
          <Link href="/">
            <a className="text-center underline text-blue-500">
              Kembali ke halaman utama
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Failed;
