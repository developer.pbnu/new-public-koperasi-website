/* eslint-disable @next/next/no-sync-scripts */
import Document, { Html, Head, Main, NextScript, Script } from "next/document";
import {appConfig} from "../configs/env";

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);

    return initialProps;
  }

  render() {
    return (
      <Html>
        <Head>
          <link
            href="https://fonts.googleapis.com/css2?family=Cinzel+Decorative:wght@700;900&family=Philosopher:wght@400;700&family=Raleway:wght@400;500&display=swap"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            type="text/javascript"
            src={appConfig.url_snap}
            data-client-key={appConfig.midtrans_client_key}
          ></script>
        </body>
      </Html>
    );
  }
}

export default MyDocument;
