import { useQuery } from "react-query";
import { http } from "../store/http";

export const useGetBlogs = () => {
  const endpoint = "/news?size=8&page=1";
  const key = "all-news";
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetSingleBlog = (slug) => {
  const endpoint = "/news/" + slug;
  const key = "single-news";
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
