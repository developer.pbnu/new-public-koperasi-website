import { useQuery } from "react-query";
import { http } from "../store/http";

export const usePaymentToken = (token) => {
  const endpoint = `/payments/registration/${token}`;
  const key = ["payment", token];
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
