import { useQuery } from "react-query";
import { http } from "../store/http";

export const useGetProvinces = () => {
  const endpoint = "/provinces";
  const key = "provinces";
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetCity = (prov) => {
  const endpoint = `/provinces/${prov}/cities`;
  const key = ["city", prov];
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};

export const useGetBirthPlace = () => {
  const endpoint = "/cities";
  const key = "birth_place";
  const fn = () => {
    return http.get(endpoint);
  };
  const { data, isSuccess, refetch, isLoading } = useQuery(key, fn);
  return { data, isSuccess, refetch, isLoading };
};
