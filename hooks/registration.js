import { useMutation } from "react-query";
import { http } from "../store/http";

export const usePostRegistration = () => {
  const endpoint = "/auth/signup";
  const key = "auth-signup";
  const fn = (data) => {
    return http.post(endpoint, data);
  };
  const { mutate, isSuccess, isError, error } = useMutation(key, (e) => fn(e));
  return { mutate, isSuccess, isError, error };
};

export const usePostChekNik = () => {
  const endpoint = "/auth/nik-available";
  const key = "nik-available";
  const fn = (data) => {
    return http.post(endpoint, data);
  };
  const { mutate, isSuccess, isError, error } = useMutation(key, (e) => fn(e));
  return { mutate, isSuccess, isError, error };
};
