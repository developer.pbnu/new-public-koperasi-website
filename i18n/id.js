export const idTranslation = {
  brand: "BUMI Nusantara",
  title: "Koperasi Konsumen Badan Usaha Mandiri Nusantara",
  description:
    "Inisiatif pengurus dan pegawai dilingkungan Nahdlatul Ulama dalam mewujudkan kemandirian dan kesejateraan bagi warga Nahdliyyin dan masyarakat sebagai anggota melalui Koperasi BUMI Nusantara.",
  menu: {
    search: "Cari",
    contact: "Kontak",
  },
  prolog:
    "Koperasi BUMI Nusantara, adalah koperasi yang bergerak di bidang usaha konsumen, berada dibawah naungan Pengurus Besar Nahdlatul Ulama.",
  public: {
    hide: "Sembunyikan",
    dataNotFound: "Data tidak ditemukan",
    viewAll: "Lihat semua",
  },
  index: {
    methodPayment: "Metode Pembayaran",
    register: "Daftar",
    registerPublic: "Untuk Umum",
    registerPrivate: "Dengan Undangan (NIK)",
    registerPrivateDesc:
      "Register ini hanya berlaku kepada mereka yang menerima undangan, baik melalui email atau telepon, atau text message.",
    visi: {
      1: "Menjadi Koperasi yang bergerak di bidang usaha konsumen",
      2: "Megembangkan Kapasitas Organisasi Dan Anggota",
      3: "Meningkatkan Kesejahteraan Dan Ekonomi Bersama",
      4: "Mewujudkan Prinsip Mabadi Khoiro Ummah",
    },
    blogs: {
      title: "Blog",
      subtitle: "Berita dan Informasi Terkini",
    },
  },
  register: {
    nik: "NIK",
    name: "Nama lengkap",
    email: "Alamat email",
    password: "Kata sandi",
    confirm_password: "Konfirmasi kata sandi",
    phone_number: "Nomor telepon",
    birth_date: "Tanggal lahir",
    birth_place: "Tempat lahir",
    gender: "Jenis kelamin",
    religion: "Agama",
    address: "Alamat lengkap",
    province: "Provinsi",
    city: "Kabupaten",
    photo_ktp: "Poto Kartu Tanda Penduduk (KTP)",
    photo_profil: "Poto Profil",
    mandatory_saving_count: "Jumlah Simpanan Wajib",
    validationSuccessNik:
      "NIK anda tervalidasi, selamat dan terima kasih, isi form dibawah ini, dan automatis akan data anda akan disetujui oleh admin.",
    success: {
      title: "Pendaftaran berhasil",
      subtitle: "Terima kasih telah mendaftar di BUMI Nusantara",
      button: "Kembali ke halaman utama",
      description:
        "Kami akan mengirimkan email konfirmasi ke alamat email anda, jika anda mengisi alamat email yang benar.",
    },
  },
  payment: {
    saveUrl: "Simpan atau share alamat URL",
    paySnap: "Pembayaran via Snap",
    copy: "Salin",
    tanks:
      "Terima Kasih, satu tahap lagi anda akan menjadi anggota Koperasi BUMI Nusantara",
    name: "Nama",
    phone: "Nomor telepon",
    registered: "Tanggal Pendaftaran",
    payClick: "Klik untuk membayar",
  },
  footer: {
    financialStatements: "Laporan Keuangan",
    accountancy: "Akuntansi",
    cashAndTransactions: "Kas dan Transaksi",
    invoice: "Faktur",
    productAndInventory: "Produk dan Inventori",
    resources: "Sumber Daya",
    blogs: "Blog",
    ebookAndWhitepaper: "Ebook dan Whitepaper",
    brosurInteraktif: "Brosur Interaktif",
    event: "Event",
  },
};
