export const englishTranslation = {
  brand: "BUMI Nusantara",
  title: "Koperasi Konsumen Badan Usaha Mandiri Nusantara",
  description:
    "The initiative of the management and employees within the Nahdlatul Ulama environment in realizing independence and welfare for Nahdliyyin residents and the community as members through the BUMI Nusantara Cooperative.",
  menu: {
    search: "Search",
    contact: "Contact",
  },
  prolog:
    "BUMI Nusantara Cooperative, is a cooperative engaged in consumer business, under the auspices of the Nahdlatul Ulama Executive Board.",
  public: {
    hide: "Hide",
    dataNotFound: "Data not found",
    viewAll: "View all",
  },
  index: {
    methodPayment: "Payment Method",
    register: "Register",
    registerPublic: "For Public",
    registerPrivate: "With Invitation (NIK)",
    registerPrivateDesc:
      "This registration is only for those who receive invitation, either through email or phone, or text message.",
    visi: {
      1: "To be a cooperative engaged in consumer business",
      2: "To develop capacity and members",
      3: "To increase welfare and economic cooperation",
      4: "To achieve the principle of Mabadi Khoiro Ummah",
    },
    blogs: {
      title: "Blog",
      subtitle: "Latest News and Information",
    },
  },
  register: {
    nik: "NIK",
    name: "Full name",
    email: "Email address",
    password: "Password",
    confirm_password: "Confirm password",
    phone_number: "Phone number",
    birth_date: "Birth date",
    birth_place: "Birth place",
    gender: "Gender",
    religion: "Religion",
    address: "Full address",
    province: "Province",
    city: "City",
    photo_ktp: "KTP",
    photo_profil: "Photo Profile",
    mandatory_saving_count: "Mandatory savings count",
    validationSuccessNik:
      "Your NIK has been validated, welcome and thank you, fill in the form below and automatically your data will be approved by the admin.",
    success: {
      title: "Registration success",
      subtitle: "Thank you for registering at BUMI Nusantara",
      button: "Back to main page",
      description:
        "We will send an email confirmation to your email address, if you fill in the correct email address.",
    },
  },
  payment: {
    saveUrl: "Save or share the address URL",
    paySnap: "Pay via Snap",
    copy: "Copy",
    tanks:
      "Thank you for your support, we will be in touch with you soon.",
    name: "Name",
    phone: "Phone",
    registered: "Registered at",
    payClick: "Clik for Pay",
  },
  footer: {
    financialStatements: "Financial Statements",
    accountancy: "Accountancy",
    cashAndTransactions: "Cash and Transactions",
    invoice: "Invoice",
    productAndInventory: "Product and Inventory",
    resources: "Resources",
    blogs: "Blogs",
    ebookAndWhitepaper: "eBook and Whitepaper",
    brosurInteraktif: "Interactive Brochure",
    event: "Events",
  },
};
