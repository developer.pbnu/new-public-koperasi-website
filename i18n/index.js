import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { englishTranslation } from "./en";
import { idTranslation } from "./id";
import LanguageDetector from "i18next-browser-languagedetector";
import { Dropdown } from "antd";
import { CaretDownOutlined } from "@ant-design/icons";

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)
const resources = {
  en: {
    translation: englishTranslation,
  },
  id: {
    translation: idTranslation,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(LanguageDetector)
  .init({
    resources,
    lng: "id", // language to use, more information here: https://www.i18next.com/overview/configuration-options#languages-namespaces-resources
    // you can use the i18n.changeLanguage function to change the language manually: https://www.i18next.com/overview/api#changelanguage
    // if you're using a language detector, do not define the lng option

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

const languages = [
  {
    name: "English",
    key: "en",
    icon: (
      // eslint-disable-next-line @next/next/no-img-element
      <img
        width={16}
        alt="United States"
        src="http://purecatamphetamine.github.io/country-flag-icons/3x2/GB.svg"
        // eslint-disable-next-line react/jsx-no-duplicate-props
      />
    ),
  },
  {
    name: "Indonesia",
    key: "id",
    icon: (
      // eslint-disable-next-line @next/next/no-img-element
      <img
        width={16}
        alt="Indonesia"
        src="http://purecatamphetamine.github.io/country-flag-icons/3x2/ID.svg"
      />
    ),
  },
];

export const SelectLang = ({ i18nHook }) => {
  const changeLanguage = (text) => {
    i18nHook.changeLanguage(text);
  };
  const languageMenu = (
    <div className="bg-white p-1 mt-1.5 rounded">
      {languages.map((prop, index) => {
        return (
          <button
            key={index}
            onClick={() => changeLanguage(prop.key)}
            className="block w-full"
          >
            <div
              className="text-xs md:text-sm bg-gray-50 p-2 rounded hover:bg-gray-200"
              style={{ display: "flex", justifyContent: "flex-start" }}
            >
              <span className="flex gap-2">
                {prop.icon}
                {prop.name}
              </span>
            </div>
          </button>
        );
      })}
    </div>
  );

  const selectedLanguageIndex = languages.findIndex(
    (prop) => prop.key === i18nHook.language
  );

  const selectedLanguage =
    selectedLanguageIndex > -1
      ? languages[selectedLanguageIndex]
      : languages[0];

  return (
    <div>
      <Dropdown overlay={languageMenu} trigger={["click"]}>
        <a
          className="text-gray-800 flex gap-2 p-0.5 flex-row justify-center items-center"
          onClick={(e) => e.preventDefault()}
        >
          {selectedLanguage.icon}
          <CaretDownOutlined style={{ color: "#f1f1f1" }} />
        </a>
      </Dropdown>
    </div>
  );
};

export default i18n;
