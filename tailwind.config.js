module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
    "./i18n/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        app: ["Arial", "sans-serif"],
        hero: ["Cinzel Decorative"],
        philosopher: ["Philosopher"],
      },
    },
  },
  plugins: [],
};
