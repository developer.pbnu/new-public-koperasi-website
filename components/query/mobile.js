import React from "react";
import { observer } from "mobx-react-lite";
import { useTranslation } from "react-i18next";
import { useActive } from "../../global/useActive";
import HeaderMobile from "../header/headerMobile";
import DrawerComponents from "../utils/drawer";

export const Mobile = observer(({ children }) => {
  const { t } = useTranslation();

  const [isOpen, setIsOpen] = React.useState(false);
  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState);
  };
  return (
    <>
      <div className="bg-white min-h-screen">
        <HeaderMobile toggleDrawer={toggleDrawer} isOpen={isOpen} />
        <DrawerComponents isOpen={isOpen} toggleDrawer={toggleDrawer} />
        {children}
      </div>
    </>
  );
});
