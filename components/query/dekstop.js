/* eslint-disable @next/next/no-img-element */
import React from "react";
import { useHotkeys } from "react-hotkeys-hook";
import DrawerComponents from "../utils/drawer";
import Search from "../utils/search";
import { observer } from "mobx-react-lite";
import HeaderDekstop from "../header/headerDekstop";

export const Dekstop = observer(({ children }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const toggleDrawer = () => {
    setIsOpen((prevState) => !prevState);
  };
  useHotkeys("ctrl+/", () => setIsModalVisible(true));
  useHotkeys("command+/", () => setIsModalVisible(true));
  useHotkeys("ctrl+k", () => setIsOpen(true));
  useHotkeys("command+k", () => setIsOpen(true));
  useHotkeys("esc", () => {
    setIsModalVisible(false);
    setIsOpen(false);
  });

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div className="w-full">
      <Search
        isModalVisible={isModalVisible}
        handleCancel={handleCancel}
        device="dekstop"
      />
      <HeaderDekstop
        isOpen={isOpen}
        setIsOpen={() => setIsOpen(!isOpen)}
        isModalVisible={isModalVisible}
        setIsModalVisible={() => setIsModalVisible(!isModalVisible)}
      />
      <DrawerComponents
        isOpen={isOpen}
        toggleDrawer={toggleDrawer}
        device="dekstop"
      />
      <div className="min-h-screen">{children}</div>
    </div>
  );
});
