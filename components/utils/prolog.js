import React from "react";
import { useTranslation } from "react-i18next";
import { useActive } from "../../global/useActive";

export const Prolog = () => {
  const { t } = useTranslation();
  return (
    <>
      <div className="bg-slate-200 py-4 px-8 gap-2">
        <p className="text-center text-xs">{t("prolog")}</p>
        <div className="flex justify-center">
          <button
            onClick={() => useActive.setActiveProlog(false)}
            className="font-bold text-red-500 text-xs"
          >
            {t("public.hide")}
          </button>
        </div>
      </div>
    </>
  );
};
