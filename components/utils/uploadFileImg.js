import React, { useState } from "react";
import { Form, Upload, Button, message, Image } from "antd";
import ImgCrop from "antd-img-crop";
import { fileRepository } from "../../hooks/file";
import {appConfig} from "../../configs/env";
const { Dragger } = Upload;

function UploadFileImg({ aspect, setValue, label, type, name }) {
    const [loading, setLoading] = useState(false);
    const [responseFile, setResponseFile] = useState("");

    const uploadHandler = async (args, type) => {
        try {
            const file = args.file;
            const res = await fileRepository.api.uploadFile(file, type);
            setValue(res.data.filename);
            setResponseFile(res.data.filename);
            setLoading(false);
            return res.data.filename;
        } catch (e) {
            message.error("Failed Upload");
        }
    };

    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
        if (!isJpgOrPng) {
            message.error("You can only upload JPG/PNG file!");
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error("Image must smaller than 2MB!");
        }
        return isJpgOrPng && isLt2M;
    };

    return (
        <>
            <Form.Item
                label={label}
                shape="circle"
                className="mb-5"
                required={true}
                name={name}
            >
                {loading && <p>loading</p>}
                <ImgCrop rotate modalTitle={label} aspect={aspect / 2} grid={true}>
                    <Dragger
                        showUploadList={false}
                        beforeUpload={beforeUpload}
                        maxCount={1}
                        customRequest={(args) => {
                            uploadHandler(args, type);
                        }}
                        className="rounded-10 px-10"
                    >
                        {responseFile ? (
                            <div className="flex justify-center">
                                <Image
                                    preview={false}
                                    className="rounded-10 w-[40%]"
                                    src={`${appConfig.apiUrl}/files/${responseFile}`}
                                    alt={type}
                                />
                                <p className="text-left ml-8 text-xs text-gray-600">
                                    {responseFile}
                                </p>
                            </div>
                        ) : (
                            <>
                                <Button type="dashed">Upload file *jpg/png</Button>
                            </>
                        )}
                    </Dragger>
                </ImgCrop>
            </Form.Item>
        </>
    );
}

export default UploadFileImg;
