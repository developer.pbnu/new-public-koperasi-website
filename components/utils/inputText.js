import React from "react";
import { Form, Input } from "antd";
const { TextArea, Password } = Input;

function InputText({
  label,
  name,
  placeholder,
  model,
  rows,
  rules,
  type = "text",
  prefix,
  dependencies,
  disabled = false,
  value,
}) {
  return (
    <Form.Item
      style={{
        marginBottom: 10,
      }}
      label={<span className="text-sm">{label}</span>}
      name={name}
      rules={rules}
      hasFeedback={type === "password" ? true : false}
    >
      {model === "textarea" ? (
        <TextArea
          autoComplete="off"
          style={{
            height: "40px",
          }}
          rows={rows}
          placeholder={placeholder}
        />
      ) : type === "password" ? (
        <Password
          autoComplete="off"
          style={{
            height: "40px",
          }}
          dependencies={dependencies}
          type={type}
          prefix={prefix}
          placeholder={placeholder}
        />
      ) : (
        <Input
          autoComplete="off"
          disabled={disabled}
          style={{
            height: "40px",
          }}
          value={value}
          type={type}
          prefix={prefix}
          placeholder={placeholder}
        />
      )}
    </Form.Item>
  );
}

export default InputText;
