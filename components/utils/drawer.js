import React from "react";
import { Drawer } from "antd";
import { useTranslation } from "react-i18next";
import Link from "next/link";

const DrawerComponents = ({ isOpen, toggleDrawer, device = "mobile" }) => {
  const { t, i18n } = useTranslation();
  return (
    <>
      <Drawer
        closeIcon={null}
        visible={isOpen}
        onClose={toggleDrawer}
        placement={device === "mobile" ? "bottom" : "right"}
      >
        <p className="text-xs text-gray-600">{t("prolog")}</p>
        <Link href="/blogs">Blogs</Link>
      </Drawer>
    </>
  );
};

export default DrawerComponents;
