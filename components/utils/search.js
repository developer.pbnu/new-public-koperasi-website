import { Input, Modal } from "antd";
import Link from "next/link";
import React from "react";
import { useTranslation } from "react-i18next";
import { HiOutlineSearch, HiX } from "react-icons/hi";
import { useGetBlogs } from "../../hooks/blogs";

const Search = ({ isModalVisible, handleCancel, device = "mobile" }) => {
  const { t } = useTranslation();
  const [searchBlogs, setSearchBlogs] = React.useState("");
  const { data: dataSearchBlogsApi, isSuccess: isSuccessBlogs } = useGetBlogs();
  const dataBlogs = isSuccessBlogs ? dataSearchBlogsApi.data : [];
  const dataSearchBlogs = dataBlogs.filter((item) => {
    return item.title.toLowerCase().includes(searchBlogs.toLowerCase());
  });
  return (
    <>
      <Modal
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={null}
        header={null}
        closable={false}
        width={device === "mobile" ? "100%" : "60%"}
        bodyStyle={{
          padding: "5px",
          borderRadius: "8px",
        }}
      >
        <Input
          onChange={(e) => setSearchBlogs(e.target.value)}
          style={{
            borderRadius: "8px",
            backgroundColor: "white",
            border: "1px solid #d9d9d9",
            fontSize: device === "mobile" ? "0.7rem" : "1.5rem",
            paddingLeft: "10px",
          }}
          size="large"
          placeholder="Search"
          prefix={<HiOutlineSearch />}
          suffix={
            <span className="text-xs font-bold text-gray-600 h-full flex items-center">
              {device === "mobile" ? (
                <button onClick={handleCancel}>
                  <HiX className="w-4 h-4" />
                </button>
              ) : (
                "ESC"
              )}
            </span>
          }
        />

        {searchBlogs.length > 0 &&
          dataSearchBlogs.map((dt, index) => {
            return (
              <div key={index} className="mt-2 cursor-pointer">
                <Link href={`/blogs/${dt?.slug}`}>
                  <a>
                    <div className="my-1 px-2">
                      <p className="text-sm font-bold">{dt?.title}</p>
                      <p className="text-xs">{dt?.content}</p>
                    </div>
                  </a>
                </Link>
              </div>
            );
          })}
        {dataSearchBlogs.length === 0 && (
          <p className="my-10 text-center text-gray-700">
            {t("public.dataNotFound")}
          </p>
        )}
      </Modal>
    </>
  );
};

export default Search;
