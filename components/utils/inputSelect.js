import React from "react";
import { Form, Select } from "antd";
const { Option } = Select;

function InputSelect({
  onChange,
  placeholder,
  data,
  label,
  name,
  rules,
  value,
  defaultValue,
  onInputKeyDown,
}) {
  return (
    <Form.Item
      style={{
        marginBottom: 10,
      }}
      label={label}
      name={name}
      rules={rules}
    >
      <Select
        showSearch
        size="large"
        placeholder={placeholder}
        defaultValue={defaultValue}
        onChange={onChange}
        style={{ width: "100%", fontSize: 14 }}
        optionFilterProp="children"
        onInputKeyDown={onInputKeyDown}
        filterOption={(input, option) =>
          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }
      >
        {data.map((item, index) => (
          <Option value={item[value]} key={index}>
            {item.name.replace("Kab.", "")}
          </Option>
        ))}
      </Select>
    </Form.Item>
  );
}

export default InputSelect;
