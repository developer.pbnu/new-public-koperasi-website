import React from "react";
import { useTranslation } from "react-i18next";

export const FooterPage = () => {
  const { t } = useTranslation();
  return (
    <div className="bg-white w-full py-10 z-40 text-xs md:text-sm">
      <div className="p-4 grid grid-cols-1 md:grid-cols-4 gap-4 max-w-6xl mx-auto">
        <div>
          <p className="font-hero text-3xl mb-2">{t("brand")}</p>
          <p className="font-app">{t('title')}</p>
          <p className="mt-10">&copy; {new Date().getFullYear()}</p>
        </div>
        <div>
          <p className="font-bold">Fitur</p>
          <p className="font-base my-4">{t('footer.financialStatements')}</p>
          <p className="font-base my-4">{t('footer.accountancy')}</p>
          <p className="font-base my-4">{t('footer.cashAndTransactions')}</p>
          <p className="font-base my-4">{t('footer.invoice')}</p>
          <p className="font-base my-4">{t('footer.productAndInventory')}</p>
        </div>
        <div>
          <p className="font-bold">{t('footer.resources')}</p>
          <p className="font-base my-4">{t('footer.blogs')}</p>
          <p className="font-base my-4">{t('footer.ebookAndWhitepaper')}</p>
          <p className="font-base my-4">{t('footer.brosurInteraktif')}</p>
          <p className="font-base my-4">{t('footer.event')}</p>
        </div>
        <div>
          <p className="font-bold">Ketentuan dan Kebijakan</p>
          <p className="font-base my-4">Informasi & Syarat keanggotaan</p>
          <p className="font-base my-4">Ketentuan & Kondisi</p>
          <p className="font-base my-4">Kebijakan Privasi</p>
        </div>
      </div>
    </div>
  );
};
