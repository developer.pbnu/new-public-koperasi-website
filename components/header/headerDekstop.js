import React from "react";
import { observer } from "mobx-react-lite";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import { HiMenuAlt3, HiOutlineSearch, HiX } from "react-icons/hi";
import { RiCommandFill } from "react-icons/ri";
import { SelectLang } from "../../i18n";

const HeaderDekstop = observer(
  ({ isOpen, setIsOpen, isModalVisible, setIsModalVisible }) => {
    const { t, i18n } = useTranslation();

    return (
      <>
        <div className="sticky top-0 z-50 w-full bg-slate-700 py-1 md:px-2 lg:px-16 shadow-lg">
          <div className="flex items-center justify-between px-4">
            <div className="flex justify-center gap-4 items-center">
              <Link href="/">
                <a className="font-bold text-lg hover:text-slate-100 font-brand text-white font-hero">
                  {t("brand")}
                </a>
              </Link>
              <div className="bg-slate-800 border-none focus:outline-none rounded flex gap-2 items-center py-1.5 px-2">
                <SelectLang i18nHook={i18n} />
              </div>
            </div>
            <Right
              isOpen={isOpen}
              setIsOpen={setIsOpen}
              isModalVisible={isModalVisible}
              setIsModalVisible={setIsModalVisible}
            />
          </div>
        </div>
      </>
    );
  }
);

const Right = ({ setIsModalVisible, isModalVisible, setIsOpen, isOpen }) => {
  const { t } = useTranslation();
  return (
    <div className="flex gap-4 items-center">
      <button
        onClick={setIsModalVisible}
        className={`border-none rounded py-1.5 px-2 flex items-center ${
          isModalVisible
            ? "bg-slate-800 hover:bg-slate-900 text-yellow-400"
            : "bg-slate-800 hover:bg-slate-900 text-slate-50"
        }`}
      >
        {isModalVisible ? <HiX /> : <HiOutlineSearch />}
        <span className="text-sm flex items-center ml-2">
          <span className="mr-2">{t("menu.search")}</span>
          <div className="flex items-center bg-slate-900 px-2 rounded-lg">
            <span className="text-xs mr-1">
              <span className="font-bold">ctrl</span> or
            </span>
            <RiCommandFill className="mr-1" /> + /
          </div>
        </span>
      </button>
      <button
        onClick={setIsOpen}
        className={`border-none rounded py-1.5 px-2 flex items-center ${
          isOpen
            ? "bg-slate-800 hover:bg-slate-900 text-yellow-400"
            : "bg-slate-800 hover:bg-slate-900 text-slate-50"
        }`}
      >
        {isOpen ? <HiX /> : <HiMenuAlt3 />}
        <span className="text-sm flex items-center ml-2">
          <span className="mr-2">Menu</span>
          <div className="flex items-center bg-slate-900 px-2 rounded-lg">
            <span className="text-xs mr-1">
              <span className="font-bold">ctrl</span> or
            </span>
            <RiCommandFill className="mr-1" /> + k
          </div>
        </span>
      </button>
    </div>
  );
};

export default HeaderDekstop;
