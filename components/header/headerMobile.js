import Link from "next/link";
import React from "react";
import { useTranslation } from "react-i18next";
import { HiMenuAlt3, HiOutlineSearch, HiX } from "react-icons/hi";
import { SelectLang } from "../../i18n";
import Search from "../utils/search";

const HeaderMobile = ({ toggleDrawer, isOpen }) => {
  const { t, i18n } = useTranslation();
  const [isModalVisible, setIsModalVisible] = React.useState(false);

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <>
      <div className="sticky top-0 z-50 left-0 w-full bg-gray-800 text-gray-50">
        <div className="flex justify-between">
          <div className="flex justify-center items-center bg-gray-900 pr-5 rounded-tr-full pl-4 py-2 gap-2">
            <Link href="/">
              <a className="font-bold font-brand capitalize text-lime-500 font-hero">
                {t("brand")}
              </a>
            </Link>
            <SelectLang i18nHook={i18n} />
          </div>
          <div className="flex gap-2 items-center pr-4 py-2">
            <button
              onClick={() => setIsModalVisible(!isModalVisible)}
              className="border bg-gray-900 bg-opacity-20 border-gray-600 rounded-lg py-1.5 px-2 flex items-center"
            >
              {isModalVisible ? <HiX /> : <HiOutlineSearch />}
              <span className="text-xs font-thin ml-2 pr-10">{t("menu.search")}</span>
            </button>
            <button
              onClick={toggleDrawer}
              className="border bg-gray-900 bg-opacity-20 border-gray-600 rounded-lg py-2 px-2 flex items-center"
            >
              <div className="flex items-center">
                {!isOpen ? (
                  <HiMenuAlt3 className="mx-auto text-gray-50 h-auto" />
                ) : (
                  <HiX className="mx-auto text-gray-50 h-auto" />
                )}
              </div>
            </button>
          </div>
        </div>
      </div>
      <Search isModalVisible={isModalVisible} handleCancel={handleCancel} />
    </>
  );
};

export default HeaderMobile;
