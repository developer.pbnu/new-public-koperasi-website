import React from "react";
import { useTranslation } from "react-i18next";
import { Image } from "antd";

export const Visi = () => {
  return (
    <div>
      <div className="block md:hidden">
        <main className="relative">
          <Data />
        </main>
      </div>
      <div className="hidden md:block">
        <main className="relative">
          <Data />
        </main>
      </div>
    </div>
  );
};

const Data = () => {
  const { t } = useTranslation();
  return (
    <>
      <div className="max-w-6xl mx-auto mt-10 p-2 md:p-0">
        <div className="grid grid-cols-2 md:grid-cols-4" data-aos="fade-up">
          <div className="w-full h-full p-1.5">
            <div className="bg-white hover:bg-lime-50 cursor-pointer h-full p-4 relative shadow-lg rounded-xl">
              <div className="flex justify-center mb-2">
                <Image
                  preview={false}
                  src="/visi/businessman-svgrepo-com.svg"
                  alt="businessman-svgrepo-com"
                  className="w-[20%] mx-auto"
                />
              </div>
              <h1 className="font-bold z-10 relative text-center">
                {t("index.visi.1")}
              </h1>
            </div>
          </div>
          <div className="w-full h-full p-1.5">
            <div className="bg-white hover:bg-lime-50 cursor-pointer h-full p-4 relative shadow-lg rounded-xl">
              <div className="flex justify-center mb-2">
                <Image
                  preview={false}
                  src="/visi/growth-svgrepo-com.svg"
                  alt="businessman-svgrepo-com"
                  className="w-[20%] mx-auto"
                />
              </div>
              <h1 className="font-bold z-10 relative text-center">
                {t("index.visi.2")}
              </h1>
            </div>
          </div>
          <div className="w-full h-full p-1.5">
            <div className="bg-white hover:bg-lime-50 cursor-pointer h-full p-4 relative shadow-lg rounded-xl">
              <div className="flex justify-center mb-2">
                <Image
                  preview={false}
                  src="/visi/organization-svgrepo-com.svg"
                  alt="businessman-svgrepo-com"
                  className="w-[20%] mx-auto"
                />
              </div>
              <h1 className="font-bold z-10 relative text-center">
                {t("index.visi.3")}
              </h1>
            </div>
          </div>
          <div className="w-full h-full p-1.5">
            <div className="bg-white hover:bg-lime-50 cursor-pointer h-full p-4 relative shadow-lg rounded-xl">
              <div className="flex justify-center mb-2">
                <Image
                  preview={false}
                  src="/visi/users-silhouettes-svgrepo-com.svg"
                  alt="businessman-svgrepo-com"
                  className="w-[20%] mx-auto"
                />
              </div>
              <h1 className="font-bold z-10 relative text-center">
                {t("index.visi.4")}
              </h1>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
