import React, { useEffect } from "react";
import { Carousel, Image } from "antd";
import { useTranslation } from "react-i18next";
import { useMediaQuery } from "react-responsive";
import aos from "aos";

export const MethodPayment = () => {
  const isMobile = useMediaQuery({ maxWidth: "767px" });
  const [active, setActive] = React.useState(false);

  useEffect(() => {
    isMobile ? setActive(true) : setActive(false);
  }, [isMobile]);
  return (
    <>
      <div className="block md:hidden">
        <main className="relative">
          <Data active={active} />
        </main>
      </div>
      <div className="hidden md:block">
        <main data-aos="fade-up" className="relative">
          <Data active={active} />
        </main>
      </div>
    </>
  );
};

const Data = ({ active }) => {
  const { t } = useTranslation();
  const logo = [
    {
      logo: "/logos/alfamartnew.svg",
      title: "Alfamart",
    },
    {
      logo: "/logos/bniva.svg",
      title: "BNI",
    },
    {
      logo: "/logos/briva.svg",
      title: "BRI",
    },
    {
      logo: "/logos/bcava.svg",
      title: "BCA",
    },
    {
      logo: "/logos/indomaretnew.svg",
      title: "indomaret",
    },
    {
      logo: "/logos/mandiriva.svg",
      title: "Mandiri",
    },
    {
      logo: "/logos/qris.svg",
      title: "Qris",
    },
    {
      logo: "/logos/mastercard.svg",
      title: "Mastercard",
    },
  ];
  return (
    <>
      <div className="max-w-3xl mx-auto mt-5 md:-mt-10">
        <div className="bg-slate-50 md:bg-slate-100 shadow-lg p-4 rounded-none md:rounded-lg relative pt-10">
          <p className="absolute left-5 -top-3 bg-gray-600 text-white py-1 px-4 rounded-full">
            {t("index.methodPayment")}
          </p>
          <Carousel
            autoplay={true}
            autoplaySpeed={2000}
            dots={false}
            infinite={true}
            speed={500}
            slidesToShow={active ? 2 : 4}
            slidesToScroll={1}
          >
            {logo.map((dt, index) => {
              return (
                <div key={index}>
                  <div className="flex justify-center">
                    <Image
                      preview={false}
                      src={dt?.logo}
                      alt={dt?.title}
                      style={{ width: "100%", height: "100%" }}
                    />
                  </div>
                </div>
              );
            })}
          </Carousel>
        </div>
      </div>
    </>
  );
};
