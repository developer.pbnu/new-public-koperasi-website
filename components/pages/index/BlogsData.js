import React from "react";
import { Image, Input, Segmented } from "antd";
import Link from "next/link";
import { useTranslation } from "react-i18next";
import { HiOutlineSearch } from "react-icons/hi";
import { useGetBlogs } from "../../../hooks/blogs";
import moment from "moment";

const BlogsData = () => {
  const [active, setActive] = React.useState(true);
  const { t } = useTranslation();

  const { data: dataBlogs, isSuccess } = useGetBlogs();
  const blogs = isSuccess ? dataBlogs.data : [];

  const [search, setSearch] = React.useState("");
  const dataSearch = blogs.filter((item) => {
    return item?.title.toLowerCase().includes(search.toLowerCase());
  });

  return (
    <>
      <div data-aos="fade-up" className="py-2 relative mt-4 max-w-6xl mx-auto">
        <div className="bg-green-600 h-5 w-20 rounded-full absolute top-2 right-5" />
        <div className="bg-white p-2 rounded-lg border border-gray-200 m-2">
          <div className="flex items-center justify-between px-2">
            <div>
              <p className="text-left text-xl font-bold mt-4">
                {t("index.blogs.title")}
              </p>
              <p className="text-left text-sm mb-4">
                {t("index.blogs.subtitle")}
              </p>
            </div>
            <div>
              <Segmented
                defaultValue="Grid"
                onChange={(e) => setActive(!active)}
                options={[
                  {
                    label: "List View",
                    value: "List",
                  },
                  {
                    label: "Grid View",
                    value: "Grid",
                  },
                ]}
              />
            </div>
          </div>
          <div className="mb-2 flex justify-start mx-2">
            <Input
              style={{
                borderRadius: "8px",
                backgroundColor: "white",
                border: "1px solid #d9d9d9",
              }}
              onChange={(e) => setSearch(e.target.value)}
              size="large"
              placeholder={t("menu.search")}
              prefix={<HiOutlineSearch />}
            />
          </div>
          <div className="p-2 mt-4">
            {active ? (
              <BlogsGrid data={dataSearch} />
            ) : (
              <BlogsList data={dataSearch} />
            )}
            {dataSearch.length === 0 && (
              <p className="mt-10 text-center text-gray-700">
                {t("public.dataNotFound")}
              </p>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default BlogsData;

const BlogsList = ({ data }) => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 grid-flow-row-dense gap-4 mt-2">
      {data.map((item, index) => {
        return (
          <div key={index} className="bg-slate-50 hover:bg-gray-100 border border-slate-200 p-2 rounded-lg shadow-lg">
            <div className="flex gap-4">
              <div className="w-1/2">
                <Image
                  preview={false}
                  src={item?.image_url}
                  className="rounded-lg"
                  width="100%"
                  height="100%"
                  alt="img"
                />
              </div>
              <div className="w-1/2">
                <Link href={`/blogs/${item?.slug}`}>
                  <a className="text-sm font-bold hover:text-green-600">{item?.title}</a>
                </Link>
                <p className="text-xs mt-2 md:text-sm">{item?.content}</p>
                <p className="mt-2 text-xs md:text-xs font-bold">
                  {item?.date}
                </p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

const BlogsGrid = ({ data }) => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-4 gap-4 mt-2">
      {data.map((item, index) => {
        return (
          <div key={index} className="bg-slate-50 hover:bg-gray-100 border border-slate-200 p-2 rounded-lg shadow-lg">
            <div>
              <Image
                preview={false}
                src={item?.image_url}
                className="rounded-lg"
                width="100%"
                height="100%"
                alt="img"
              />
            </div>
            <div className="mt-2">
              <Link href={`/blogs/${item?.slug}`}>
                <a className="text-sm font-bold hover:text-green-600">{item?.title}</a>
              </Link>
              <p className="text-xs mt-2 md:text-sm">{item?.excerpt}</p>
              <p className="mt-2 text-xs md:text-xs font-bold">
                {moment(item?.date).fromNow()}
              </p>
            </div>
          </div>
        );
      })}
    </div>
  );
};
