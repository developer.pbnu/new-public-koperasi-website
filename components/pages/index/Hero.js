import React, { useState } from "react";
import { Button, Carousel, Dropdown, Form, Image, message, Modal } from "antd";
import { useTranslation } from "react-i18next";
import InputText from "../../utils/inputText";
import { usePostChekNik } from "../../../hooks/registration";
import { useRouter } from "next/router";
import { useNik } from "../../../global/useNik";

export const Hero = () => {
  return (
    <>
      <div className="block md:hidden">
        <main className="relative">
          <div className="patt py-5 md:py-20 px-2 md:px-10">
            <Data />
          </div>
        </main>
      </div>
      <div className="hidden md:block">
        <main data-aos="fade-down" className="relative">
          <div className="patt py-5 md:py-20 px-2 md:px-10">
            <Data />
          </div>
        </main>
      </div>
    </>
  );
};

const Data = () => {
  const { t } = useTranslation();
  const router = useRouter();
  const [modalReg, setModalReg] = React.useState(false);
  const image = [
    {
      image: "/images/ketum.jpeg",
      name: "KH. Yahya Cholil Staquf ",
      text: "Kita ini sangat dibutuhkan oleh semua pihak. Kita harus berani bekerja sama dengan semua pihak. Kita harus berani untuk mengajak semua pihak ikut bekerja untuk membangun kapasitas sosial-ekonomi dari warga NU supaya lebih kuat",
    },
    {
      image: "/images/rois.jpeg",
      name: "KH. Miftahul Akhyar ",
      text: "Kita ini sangat dibutuhkan oleh semua pihak. Kita harus berani bekerja sama dengan semua pihak. Kita harus berani untuk mengajak semua pihak ikut bekerja untuk membangun kapasitas sosial-ekonomi dari warga NU supaya lebih kuat",
    },
  ];
  const regPublic = () => {
    useNik.setNik("");
    router.push("/register");
  };
  return (
    <>
      <ModalRegister modalReg={modalReg} onCancel={() => setModalReg(false)} />
      <div className="w-full md:max-w-[1300px] block md:flex md:justify-between mx-auto">
        <div data-aos="fade-down" className="px-2 w-full md:w-2/3">
          <h1 className="text-xl px-10 md:px-0 mb-4 md:text-3xl text-center md:text-left font-hero text-green-700">
            Koperasi Konsumen <br /> Badan Usaha Mandiri Nusantara
          </h1>
          <p className="text-center text-green-700 md:text-left mt-2 text-sm md:text-lg">
            {t("description")}
          </p>
          <div className="mt-4 flex justify-center md:justify-start w-full">
            <div>
              <Dropdown
                overlay={
                  <div className="rounded-lg p-1 bg-white shadow-lg border border-slate-200 mt-2">
                    <div className="py-1 hover:bg-gray-100 p-1">
                      <button onClick={regPublic}>
                        <a className="hover:text-green-600 w-full">
                          {t("index.registerPublic")}
                        </a>
                      </button>
                    </div>
                    <div className="py-1 hover:bg-gray-100 p-1">
                      <button onClick={() => setModalReg(true)}>
                        <a className="hover:text-green-600 w-full">
                          {t("index.registerPrivate")}
                        </a>
                      </button>
                    </div>
                  </div>
                }
                trigger={["click"]}
              >
                <div className="py-2 px-6 rounded bg-green-600 hover:bg-green-800 font-bold text-white shadow-lg w-max cursor-pointer">
                  <span className="uppercase tracking-widest">
                    {t("index.register")}
                  </span>
                </div>
              </Dropdown>
            </div>
          </div>
        </div>
        <div
          data-aos="fade-down"
          className="flex justify-center items-center w-full md:w-1/3 mt-5"
        >
          <div className="bg-white p-2 w-full rounded-3xl bg-opacity-50">
            <div className="bg-white p-4 rounded-3xl rounded-tr-[150px] relative border border-slate-200 shadow-lg">
              <div className="w-20 h-20 absolute top-0 rounded-tl-3xl rounded-br-[100px] left-0 bg-green-600" />
              <Carousel
                autoplay={true}
                autoplaySpeed={5000}
                dots={false}
                infinite={true}
                speed={500}
                slidesToShow={1}
                slidesToScroll={1}
              >
                {image.map((dt, index) => {
                  return (
                    <div key={index} className="w-40">
                      <div className="w-full flex justify-center mb-5">
                        <Image
                          preview={false}
                          src={dt?.image}
                          alt={dt?.title}
                          className="rounded-full rounded-tl h-32 w-32 object-cover shadow-lg"
                        />
                      </div>
                      <h1 className="mt-1 font-bold text-center">{dt?.name}</h1>
                      <p className="text-xs text-center">{dt?.text}</p>
                    </div>
                  );
                })}
              </Carousel>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const ModalRegister = ({ onCancel, modalReg }) => {
  const router = useRouter();
  const { t } = useTranslation();
  const [registerForm] = Form.useForm();
  const { mutate } = usePostChekNik();
  const onFinish = async () => {
    try {
      const values = await registerForm.validateFields();
      const formData = {
        nik: parseInt(values?.nik),
      };
      mutate(formData, {
        onSuccess: (e) => {
          if (e?.data?.is_available) {
            message.success("NIK anda tersedia, dan tervalidasi");
            useNik.setNik(values?.nik);
            router.push("/register");
          } else {
            message.success(t("public.dataNotFound"));
            registerForm.resetFields();
          }
        },
        onError: (error) => {
          console.log(error);
        },
      });
    } catch (e) {}
  };
  return (
    <>
      <Modal
        title={`${t("index.register")} ${t("index.registerPrivate")} `}
        visible={modalReg}
        onCancel={onCancel}
        footer={null}
      >
        <div>
          <p className="text-xs md:text-sm text-gray-500">
            {t("index.registerPrivateDesc")}
          </p>
          <Form
            form={registerForm}
            name="register-invitation"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            layout={"vertical"}
            autoComplete="off"
          >
            <InputText
              label={
                <p className="text-xs -mb-2">Nomor Induk Kependudukan (NIK)</p>
              }
              name="nik"
              placeholder={t("register.nik")}
              rules={[
                {
                  required: true,
                  message: t("Tidak boleh kosong"),
                },
                {
                  pattern: /^(?:\d*)$/,
                  message: "Just number value",
                },
                {
                  min: 16,
                  max: 16,
                  message: "NIK number must be 16 digits",
                },
              ]}
            />
            <Button
              type="primary"
              size="large"
              htmlType="submit"
              className="bg-gray-600 hover:bg-gray-800 text-white rounded w-full"
            >
              Submit
            </Button>
          </Form>
        </div>
      </Modal>
    </>
  );
};
