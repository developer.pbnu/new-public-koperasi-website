import React from "react";
import InputSelect from "../../utils/inputSelect";

export const Step3 = ({ setmandatory_saving_count }) => {
  return (
    <>
      <InputSelect
        label="Pilihan Simpanan Wajib"
        value="value"
        name="saving"
        placeholder="Simpanan Wajib"
        data={[
          {
            value: 1,
            name: "1 Bulan (Rp. 50.000)",
          },
          {
            value: 3,
            name: "3 Bulan (Rp. 150.000)",
          },
          {
            value: 6,
            name: "6 Bulan (Rp. 300.000)",
          },
          {
            value: 12,
            name: "12 Bulan (Rp. 600.000)",
          },
        ]}
        onChange={setmandatory_saving_count}
        rules={[
          {
            required: true,
            message: "Please select your provinces",
          },
        ]}
      />
    </>
  );
};
