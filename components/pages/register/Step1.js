import React from "react";
import { useTranslation } from "react-i18next";
import InputSelect from "../../utils/inputSelect";
import InputText from "../../utils/inputText";

export const Step1 = ({ setGender, nik }) => {
  const { t } = useTranslation();
  return (
    <>
      <InputText
        label={t("register.name")}
        name="name"
        placeholder={t("register.name")}
        rules={[{ required: true, message: "Please input your full name" }]}
      />
      <InputText
        label={t("register.nik")}
        name="nik"
        disabled={nik ? true : false}
        placeholder={nik ? nik : t("register.nik")}
        rules={[
          nik
            ? { required: false }
            : {
                required: true,
                message: "Please input your NIK number",
              },
          {
            pattern: /^(?:\d*)$/,
            message: "Just number value",
          },
          {
            min: 16,
            max: 16,
            message: "NIK number must be 16 digits",
          },
        ]}
      />
      <InputText
        label={t("register.email")}
        name="email"
        placeholder={t("register.email")}
        rules={[
          {
            required: true,
            message: "Please input your Email Address",
          },
          {
            type: "email",
            message: "Enter a valid email address",
          },
        ]}
      />
      <InputText
        type="password"
        label={t("register.password")}
        name="password"
        placeholder={t("register.password")}
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      />
      <InputText
        type="password"
        label={t("register.confirm_password")}
        name="confirm_password"
        placeholder={t("register.confirm_password")}
        rules={[
          {
            required: true,
            message: "Please confirm your password!",
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                new Error("The two passwords that you entered do not match!")
              );
            },
          }),
        ]}
      />
      <InputText
        label={t("register.phone_number")}
        name="phone_number"
        placeholder={t("register.phone_number")}
        prefix="+62"
        rules={[
          {
            required: true,
            message: "Please input your Phone number",
          },
          {
            min: 10,
            max: 14,
            message: "Please input Valid Phone number",
          },
        ]}
      />
      <InputSelect
        label={t("register.gender")}
        value="value"
        name="gender"
        placeholder={t("register.gender")}
        data={[
          {
            value: "male",
            name: "Laki-laki",
          },
          {
            value: "female",
            name: "Perempuan",
          },
        ]}
        onChange={setGender}
        rules={[
          {
            required: true,
            message: "Please select your provinces",
          },
        ]}
      />
    </>
  );
};
