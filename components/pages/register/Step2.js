import React from "react";
import { useTranslation } from "react-i18next";
import InputText from "../../utils/inputText";
import InputSelect from "../../utils/inputSelect";

export const Step2 = ({
  birthPlace,
  setBirth_place,
  prov,
  setProv,
  city,
  setCityCode,
  successCity,
  successProvinces,
}) => {
  const { t } = useTranslation();
  return (
    <>
      <InputText
        type="date"
        label={t("register.birth_date")}
        name="birth_date"
        placeholder={t("register.birth_date")}
        rules={[
          {
            required: true,
            message: "Please input your birth date",
          },
        ]}
      />
      {birthPlace && (
        <InputSelect
          label={t("register.birth_place")}
          name="birth_place"
          placeholder={t("register.birth_place")}
          data={birthPlace.data}
          onChange={setBirth_place}
          rules={[
            {
              required: true,
              message: "Please select your birth place",
            },
          ]}
        />
      )}
      {successProvinces && (
        <InputSelect
          label={t("register.province")}
          name="province"
          value="code"
          placeholder={t("register.province")}
          data={prov.data}
          onChange={setProv}
          rules={[
            {
              required: true,
              message: "Please select your province",
            },
          ]}
        />
      )}
      {successCity && (
        <InputSelect
          label={t("register.city")}
          name="city"
          value="code"
          placeholder={t("register.city")}
          data={city.data}
          onChange={setCityCode}
          rules={[
            {
              required: true,
              message: "Please select your city",
            },
          ]}
        />
      )}
      <InputText
        label={t("register.address")}
        name="address"
        placeholder={t("register.address")}
        rules={[
          {
            required: true,
            message: "Please input your address",
          },
        ]}
      />
    </>
  );
};
