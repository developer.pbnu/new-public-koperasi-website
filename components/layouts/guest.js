import React from "react";
import { observer } from "mobx-react-lite";
import Link from "next/link";
import { SelectLang } from "../../i18n";
import { useTranslation } from "react-i18next";

const LayoutGuest = observer(({ children }) => {
  const { t } = useTranslation();
  return (
    <div className="bg-gray-100">
      <div className="sticky top-0 z-50 w-full bg-slate-700 py-1 md:px-2 lg:px-16 shadow-lg">
        <div className="flex justify-center items-center py-2 gap-2">
          <Link href="/">
            <a className="font-bold font-brand capitalize text-lime-500 font-hero">
              {t("brand")}
            </a>
          </Link>
        </div>
      </div>
      <div className="p-4">{children}</div>
    </div>
  );
});

export default LayoutGuest;
