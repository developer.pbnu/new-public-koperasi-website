import React from "react";
import { Dekstop } from "../query/dekstop";
import { Mobile } from "../query/mobile";
import { FooterPage } from "../footer/index";
import { observer } from "mobx-react-lite";
import { Prolog } from "../utils/prolog";
import { useActive } from "../../global/useActive";

const LayoutApp = observer(({ children }) => {
  return (
    <div className="bg-gray-100">
      {useActive.activeProlog && <Prolog />}
      <div className="block md:hidden">
        <Mobile>
          <main className="relative">{children}</main>
        </Mobile>
      </div>
      <div className="hidden md:block">
        <Dekstop>
          <main className="relative">{children}</main>
        </Dekstop>
      </div>
      <FooterPage />
    </div>
  );
});

export default LayoutApp;
