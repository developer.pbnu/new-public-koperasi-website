import { action, observable } from "mobx";

export const useActive = observable({
  activeProlog: true,
  setActiveProlog: action((e) => {
    useActive.activeProlog = e;
  }),
});
