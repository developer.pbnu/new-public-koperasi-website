import { action, observable } from "mobx";

export const useNik = observable({
  nik: "",
  setNik: action((e) => {
    useNik.nik = e;
  }),
});
